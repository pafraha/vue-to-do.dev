import { createApp } from 'vue';
import App from './App.vue';
import Vuex from 'vuex';

// Font Awesome for Vue JS
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

// Bootstrap style
import 'bootstrap/dist/css/bootstrap.min.css';

// Boostrap script with its dependencies
import '@popperjs/core';
import 'bootstrap/dist/js/bootstrap';

// SweetAlert2 for vue
import Swal from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import 'sweetalert2/dist/sweetalert2.all.min';

// Vue Notifications alert
import Notifications from '@kyvg/vue3-notification';

// Import Font Awesome and its specific components for this project VueJS
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(fas);

const app = createApp(App);

app.use(Swal);
app.use(Notifications);
app.use(Vuex);

app
    .component('fa', FontAwesomeIcon)
    .mount('#app');
